<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Periksa perangkatmuKlik notifikasi di p_d91e48</name>
   <tag></tag>
   <elementGuidId>119ce21f-7e37-4798-999e-773e9b5bf34d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-vpkyow > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='zeus-header']/div[3]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Periksa perangkatmuKlik notifikasi di perangkatmu atau pusat notifikasi dalam aplikasi Tokopedia, dan berikan akses untuk masuk ke akunmu:Lenovo A7010a48Infinix X682BBuka aplikasi Tokopedia agar notifikasi lebih mudah terkirim.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;zeus-header&quot;)/div[@class=&quot;css-hbk9v1&quot;]/div[@class=&quot;css-xyx0a6&quot;]/div[@class=&quot;css-vpkyow&quot;]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='zeus-header']/div[3]/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='racun tikus'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='spray gun'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
