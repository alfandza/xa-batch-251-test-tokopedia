import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Normal Test/TC_LOGIN_001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_Nomor HP atau Email_email-phone'), 
    'alfandza@mail.com')

WebUI.click(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/span_Selanjutnya'))

WebUI.setEncryptedText(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_Kata Sandi_password-input'), 
    '9c4gqFmD26YiY3nJ3VDJIA==')

WebUI.click(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/span_Masuk'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Notifikasi ke perangkatmu'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_SMS ke --359'), 
    0)

WebUI.click(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Notifikasi ke perangkatmu'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Periksa perangkatmuKlik notifikasi di p_d91e48'), 
    0)

WebUI.delay(5)

WebUI.navigateToUrl('https://www.tokopedia.com/')

WebUI.verifyElementPresent(findTestObject('Object Repository/Login/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Alfandza'), 
    0)

