import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC_HOME_001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/TC_SEARCH_013/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_Kategori_css-4eg0ft e16vycsw0_1'), 
    'Laptop')

WebUI.click(findTestObject('Object Repository/TC_SEARCH_013/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/button_Kategori_css-1ymn4im e1v0ehno1'))

WebUI.click(findTestObject('Object Repository/TC_SEARCH_013/Page_Jual Laptop  Tokopedia/span_Lokasi_css-12a5v84-unf-checkbox__area _d9e3d8'))

WebUI.verifyElementPresent(findTestObject('Object Repository/TC_SEARCH_013/Page_Jual Laptop  Tokopedia/div_Menampilkan 1 - 60 barang dari total 28_273c7b'), 
    0)

